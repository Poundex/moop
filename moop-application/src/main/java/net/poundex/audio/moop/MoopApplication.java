package net.poundex.audio.moop;

import javafx.application.Platform;
import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.engine.MoopEngine;
import net.poundex.audio.moop.jack.JackAudioSystem;
import net.poundex.audio.moop.ui.MoopUiApplication;
import net.poundex.audio.moop.ui.UiContext;

@Slf4j
public class MoopApplication {
    public static void main(String[] args) {
        log.info("Moop");

        MoopEngine engine = new MoopEngine(
                new JackAudioSystem());

        UiContext.INSTANCE.setEngineContext(engine);
        UiContext.INSTANCE.init();

        MoopUiApplication.launch(MoopUiApplication.class, args);

        Platform.exit();
        engine.stop();
        System.exit(0);
    }
}
