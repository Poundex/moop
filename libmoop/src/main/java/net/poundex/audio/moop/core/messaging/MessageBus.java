package net.poundex.audio.moop.core.messaging;

import net.poundex.audio.moop.core.messaging.listener.*;

public interface MessageBus {
    void addGlobalTimeListener(GlobalTimeListener listener);
    void addLoopLengthChangedListener(LoopLengthChangedListener listener);
    void addLoopTimeListener(LoopTimeListener listener);
    void addMeterChangedListener(MeterChangedListenenr listener);
    void addMidiDataEventListener(MidiDataEventListener listener);
    void addMidiPerformanceEventListener(MidiPerformanceEventListener listener);
    void addPartRecordCueListener(PartRecordCueListener listener);
    void addPartRecordStartListener(PartRecordStartListener listener);
    void addPartRecordFinishListener(PartRecordFinishListener listener);
    void addClockStartListener(ClockStartListener listener);
    void addClockStopListener(ClockStopListener listener);
    void addPartRenamedListener(PartRenamedListener listener);
    void addMidiInChannelChangedListener(MidiInChannelChangedListener listener);
    void addMidiOutChannelChangedListener(MidiOutChannelChangedListener listener);
}
