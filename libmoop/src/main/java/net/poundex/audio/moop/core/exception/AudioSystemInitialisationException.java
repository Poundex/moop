package net.poundex.audio.moop.core.exception;

public class AudioSystemInitialisationException extends InitialisationException {
    private final String audioSystemName;
    private final Throwable cause;

    public AudioSystemInitialisationException(String audioSystemName, Throwable cause) {
        super(cause);
        this.audioSystemName = audioSystemName;
        this.cause = cause;
    }

    @Override
    public String getUserMessage() {
        return String.format("Could not initialise audio system '%s':%n%s: %s", audioSystemName, cause.getClass().getSimpleName(), cause.getMessage());
    }
}
