package net.poundex.audio.moop.core.messaging.listener;

public interface MidiClockEventListener {
    void onMidiClockEvent(byte[] data, int size);
}
