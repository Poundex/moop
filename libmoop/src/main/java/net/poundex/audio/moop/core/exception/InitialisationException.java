package net.poundex.audio.moop.core.exception;

public abstract class InitialisationException extends Exception {
    public InitialisationException(Throwable cause) {
        super(cause);
    }

    public abstract String getUserMessage();
}
