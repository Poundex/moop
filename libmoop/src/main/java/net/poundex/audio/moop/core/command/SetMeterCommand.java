package net.poundex.audio.moop.core.command;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.audio.moop.core.Command;
import net.poundex.audio.moop.core.Meter;

@RequiredArgsConstructor
@Data
public class SetMeterCommand implements Command {
    private final Meter newMeter;
}
