package net.poundex.audio.moop.core;

import lombok.Data;

@Data
public class Meter {
    public final static int TICKS_PER_BEAT = 1920;

    private final double tempo;
    private final int beatsPerBar;
    private final int beatLength;
    private final int ticksPerBar;

    public Meter(double tempo, int beatsPerBar, int beatLength) {
        this.tempo = tempo;
        this.beatsPerBar = beatsPerBar;
        this.beatLength = beatLength;
        this.ticksPerBar = TICKS_PER_BEAT * beatsPerBar * (4 / beatLength);
    }

    public double getPulsePeriod() { // TODO: Remove
        return (1 / (tempo / 60)) / 24;
    }
}
