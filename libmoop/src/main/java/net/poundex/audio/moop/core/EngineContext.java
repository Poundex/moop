package net.poundex.audio.moop.core;

import net.poundex.audio.moop.core.exception.InitialisationException;
import net.poundex.audio.moop.core.looper.Looper;
import net.poundex.audio.moop.engine.WritableMessageBus;

public interface EngineContext {
    void start() throws InitialisationException;
    void stop();

    void sendCommand(Command command);

    AudioSystem getAudioSystem();
    Looper getLooper();
    WritableMessageBus getMessageBus();
}
