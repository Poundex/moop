package net.poundex.audio.moop.core.messaging.listener;

public interface PartRecordFinishListener {
    void onPartRecordFinish(int partId);
}
