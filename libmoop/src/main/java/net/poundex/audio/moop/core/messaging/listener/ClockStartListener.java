package net.poundex.audio.moop.core.messaging.listener;

public interface ClockStartListener {
    void onClockStart();
}
