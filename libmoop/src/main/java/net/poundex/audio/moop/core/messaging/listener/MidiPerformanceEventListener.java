package net.poundex.audio.moop.core.messaging.listener;

public interface MidiPerformanceEventListener {
    void onMidiPerformanceEvent(int absoluteTick, int channel, byte[] data);
}
