package net.poundex.audio.moop.core.messaging.listener;

public interface MidiOutChannelChangedListener {
    void onMidiOutChannelChanged(int partId, int newChannel);
}
