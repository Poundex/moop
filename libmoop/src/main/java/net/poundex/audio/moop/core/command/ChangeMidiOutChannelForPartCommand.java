package net.poundex.audio.moop.core.command;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.audio.moop.core.Command;

@Data
@RequiredArgsConstructor
public class ChangeMidiOutChannelForPartCommand implements Command {
    private final int partId;
    private final int newChannel;
}
