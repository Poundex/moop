package net.poundex.audio.moop.core.messaging.listener;

public interface PartRenamedListener {
    void onPartRenamed(int partId, String newName);
}
