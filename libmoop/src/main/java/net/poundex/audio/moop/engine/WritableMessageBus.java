package net.poundex.audio.moop.engine;

import net.poundex.audio.moop.core.Meter;
import net.poundex.audio.moop.core.messaging.MessageBus;

public interface WritableMessageBus extends MessageBus {
    void fireGlobalTime(long startTick, int range);
    void fireLoopLengthChanged(int loopLength);
    void fireLoopTime(int tickInLoop, boolean newLoop, int barInLoop, int beatInBar, int tickInBeat, int range);
    void fireMeterChanged(Meter meter);
    void fireMidiDataEvent(int absoluteTick, byte[] data, int size);
    void fireMidiPerformanceEvent(int absoluteTick, int channel, byte... data);
    void firePartRecordCue(int partId);
    void firePartRecordStart(int partId);
    void firePartRecordFinish(int partId);
    void fireClockStart();
    void fireClockStop();
    void firePartRenamed(int partId, String newName);
    void fireMidiInChannelChanged(int partId, int newChannel);
    void fireMidiOutChannelChanged(int partId, int newChannel);
}
