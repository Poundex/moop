package net.poundex.audio.moop.engine.util;

import lombok.Data;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.stream.IntStream;

public class ObjectPool {

    public static final Queue<QueuedMidiEvent> midiEvents
            = new ArrayDeque<>(1024);

    public static final Queue<RawInboundEvent> rawInboundEvent
            = new ArrayDeque<>(1024);

    static {
        IntStream.range(0, 1024).forEach(ignored ->
                midiEvents.add(new QueuedMidiEvent()));

        IntStream.range(0, 1024).forEach(ignored ->
                rawInboundEvent.add(new RawInboundEvent()));
    }

    @Data
    public static class QueuedMidiEvent {
        private double tickOffset;
        private byte[] data;

        private QueuedMidiEvent() { }

        public QueuedMidiEvent rehydrate(double tickOffset, byte[] data) {
            this.tickOffset = tickOffset;
            this.data = data;
            return this;
        }
    }

    @Data
    public static class RawInboundEvent {
        private int tickInLoop;
        private int channel;
        private byte[] data;

        private RawInboundEvent() { }

        public RawInboundEvent rehydrate(int tickInLoop, int channel, byte[] data) {
            this.tickInLoop = tickInLoop;
            this.channel = channel;
            this.data = data;
            return this;
        }
    }

}
