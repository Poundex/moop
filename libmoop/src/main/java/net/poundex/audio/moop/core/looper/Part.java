package net.poundex.audio.moop.core.looper;

public interface Part {
    int getId();
    String getName();
    void setName(String name);
    int getInputChannel();
    void setInputChannel(int newChannel);
    int getOutputChannel();
    void setOutputChannel(int newChannel);

    void setCue(boolean cue);
    void setRecording(boolean recording);
}
