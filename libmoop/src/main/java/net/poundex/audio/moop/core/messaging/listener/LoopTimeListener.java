package net.poundex.audio.moop.core.messaging.listener;

public interface LoopTimeListener {
    void onLoopTime(int tickInLoop, boolean newLoop, int barInLoop, int beatInBar, int tickInBeat, int range);
}
