package net.poundex.audio.moop.core.messaging.listener;

public interface GlobalTimeListener {
    void onGlobalTimeEvent(long startTick, int range);
}
