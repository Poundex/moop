package net.poundex.audio.moop.core.messaging.listener;

import net.poundex.audio.moop.core.Meter;

public interface MeterChangedListenenr {
    void onMeterChanged(Meter meter);
}
