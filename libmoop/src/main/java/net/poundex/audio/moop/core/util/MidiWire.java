package net.poundex.audio.moop.core.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MidiWire {
    public static final byte STATUS_CLOCK_PULSE = (byte) 0xF8;
    public static final byte STATUS_CLOCK_START = (byte) 0xFA;
    public static final byte STATUS_CLOCK_CONTINUE = (byte) 0xFB;
    public static final byte STATUS_CLOCK_STOP = (byte) 0xFC;

    public static final byte STATUS_NOTE_OFF = (byte) 0x80;
    public static final byte STATUS_NOTE_ON = (byte) 0x90;
    public static final byte STATUS_AFTER_TOUCH = (byte) 0xA0;
    public static final byte STATUS_CC = (byte) 0xB0;
    public static final byte STATUS_CH_PRESSURE = (byte) 0xD0;
    public static final byte STATUS_PITCH_BEND = (byte) 0xE0;

    public static final byte CC_ALL_NOTES_OFF = (byte) 0x7B;
    public static final byte CC_ALL_SOUND_OFF = (byte) 0x78;
    public static final byte CC_ALL_CONTROLLERS_RESET = (byte) 0x79;

    public static final byte MASK_CHANNEL_FROM_STATUS = 0x0F;
    public static final byte MASK_NORMALISE_STATUS = (byte) 0xF0;

    public static byte channelise(byte status, byte channelIndex) {
        return (byte) (status | channelIndex);
    }

    public static byte normalise(byte statusAndChannel) {
        return (byte) (statusAndChannel & MASK_NORMALISE_STATUS);
    }

    public static byte channel(byte statusAndChannel) {
        return (byte) (statusAndChannel & MASK_CHANNEL_FROM_STATUS);

    }
}
