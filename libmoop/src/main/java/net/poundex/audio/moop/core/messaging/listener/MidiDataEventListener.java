package net.poundex.audio.moop.core.messaging.listener;

public interface MidiDataEventListener {
    void onMidiDataEvent(int absoluteTick, byte[] data, int size);
}
