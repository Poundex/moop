package net.poundex.audio.moop.core.messaging.listener;

public interface PartRecordStartListener {
    void onPartRecordStart(int partId);
}
