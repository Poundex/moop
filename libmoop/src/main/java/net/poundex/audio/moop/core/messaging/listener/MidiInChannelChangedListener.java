package net.poundex.audio.moop.core.messaging.listener;

public interface MidiInChannelChangedListener {
    void onMidiInChannelChanged(int partId, int newChannel);
}
