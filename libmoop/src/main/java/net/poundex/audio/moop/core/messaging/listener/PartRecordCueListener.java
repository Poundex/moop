package net.poundex.audio.moop.core.messaging.listener;

public interface PartRecordCueListener {
    void onPartRecordCue(int partId);
}
