package net.poundex.audio.moop.core.command;

import net.poundex.audio.moop.core.Command;

public enum TransportStopCommand implements Command {
    INSTANCE;
}
