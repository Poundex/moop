package net.poundex.audio.moop.core.messaging.listener;

public interface LoopLengthChangedListener {
    void onLoopLengthChanged(int loopLength);
}
