package net.poundex.audio.moop.core.looper;

import java.util.List;

public interface Looper {
    List<Part> getParts();

    Part getPart(int partId);
}
