package net.poundex.audio.moop.core;

import net.poundex.audio.moop.core.exception.AudioSystemInitialisationException;

public interface AudioSystem {
    void start(EngineContext moopEngine) throws AudioSystemInitialisationException;
    void stop();

    void transportStart();
    void transportStop();

    void playMidi(byte[] data, double tick);
}
