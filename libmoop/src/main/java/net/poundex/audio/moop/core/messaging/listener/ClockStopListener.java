package net.poundex.audio.moop.core.messaging.listener;

public interface ClockStopListener {
    void onClockStop();
}
