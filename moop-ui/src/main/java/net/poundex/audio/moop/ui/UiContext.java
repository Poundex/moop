package net.poundex.audio.moop.ui;

import javafx.application.Platform;
import lombok.Getter;
import lombok.Setter;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.messaging.listener.LoopTimeListener;
import net.poundex.audio.moop.ui.model.service.PartModelService;
import net.poundex.audio.moop.ui.util.AnimationController;

import java.util.ArrayList;
import java.util.List;

public enum UiContext implements LoopTimeListener {

    INSTANCE;

    @Getter @Setter
    private EngineContext engineContext;

    @Getter
    private PartModelService partModelService;
    @Getter
    private AnimationController animationController;

    public void init() {
        this.partModelService = new PartModelService(engineContext);
        this.animationController = new AnimationController(engineContext);
    }

    int lastBeat = -1;
    @Override
    public void onLoopTime(int tickInLoop, boolean newLoop, int barInLoop, int beatInBar, int tickInBeat, int range) {
//        if(true) return;
//        if(pulseInBeat != 0) return;
        if(beatInBar == lastBeat)
            return;

        // TODO: Double Queue
        Platform.runLater(() ->
                loopTimeListeners.forEach(l ->
                        l.onLoopTime(tickInLoop, newLoop, barInLoop, beatInBar, tickInBeat, range)));

        lastBeat = beatInBar;
    }

    private final List<LoopTimeListener> loopTimeListeners = new ArrayList<>();

    public void addAsyncLoopTimeListener(LoopTimeListener listener) {
        loopTimeListeners.add(listener);
    }
}
