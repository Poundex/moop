package net.poundex.audio.moop.ui.layout;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.TransportStartCommand;
import net.poundex.audio.moop.core.command.TransportStopCommand;
import net.poundex.audio.moop.core.messaging.listener.ClockStartListener;
import net.poundex.audio.moop.core.messaging.listener.ClockStopListener;
import net.poundex.audio.moop.ui.UiContext;
import net.poundex.audio.moop.ui.control.LoopDisplay;

import java.util.stream.IntStream;


public class ControlPanel extends BorderPane implements ClockStartListener, ClockStopListener {

    private final EngineContext engineContext;
    private final SimpleBooleanProperty transportIsRolling = new SimpleBooleanProperty();

    public ControlPanel(EngineContext engineContext) {
        this.engineContext = engineContext;
        initLayout();
        engineContext.getMessageBus().addClockStartListener(this);
        engineContext.getMessageBus().addClockStopListener(this);
    }

    private void initLayout() {
        Button transportStart = new Button("START");
        transportStart.setOnAction(ae -> engineContext.sendCommand(TransportStartCommand.INSTANCE));
        transportStart.disableProperty().bind(transportIsRolling);

        Button transportStop = new Button("STOP");
        transportStop.setOnAction(ae -> engineContext.sendCommand(TransportStopCommand.INSTANCE));
        transportStop.disableProperty().bind(transportIsRolling.not());

        setLeft(new VBox(new HBox(transportStart, transportStop), new Button("4/4 @ 120bpm"), new BeatIndicator(engineContext, 4)));
        setCenter(renderGroupControls());

        GridPane throughPane = new GridPane();
        throughPane.add(new Label("Through"), 0, 0, 4, 1);
        IntStream.range(1, 17).forEach(ch -> {
            int col = (ch - 1) % 4;
            int row = (ch - 1) / 4;
            throughPane.add(new Button("" + ch), col, row + 1);
        });

        setRight(throughPane);
        LoopDisplay loopDisplay = new LoopDisplay();
//        engineContext.getMessageBus().addLoopTimeListener(loopDisplay);
        UiContext.INSTANCE.addAsyncLoopTimeListener(loopDisplay);
        setBottom(loopDisplay);
    }

    private Node renderGroupControls() {
        GridPane gp = new GridPane();

        gp.add(new Label("All"), 0, 0, 2, 1);
        gp.add(new Button("M"), 0, 1);
        gp.add(new Button("U"), 1, 1);

        gp.add(new Label("B + D"), 2, 0, 2, 1);
        gp.add(new Button("M"), 2, 1);
        gp.add(new Button("S"), 3, 1);

        gp.add(new Label("Firsts"), 4, 0, 2, 1);
        gp.add(new Button("M"), 4, 1);
        gp.add(new Button("S"), 5, 1);

        gp.add(new Label("Seconds"), 6, 0, 2, 1);
        gp.add(new Button("M"), 6, 1);
        gp.add(new Button("S"), 7, 1);

        return gp;
    }

    @Override
    public void onClockStart() {
        transportIsRolling.setValue(true);
    }

    @Override
    public void onClockStop() {
        transportIsRolling.setValue(false);
    }
}
