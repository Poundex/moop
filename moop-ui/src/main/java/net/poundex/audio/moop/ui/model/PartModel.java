package net.poundex.audio.moop.ui.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PartModel {
    @Getter
    private final int id;
    private final StringProperty name;
    private final IntegerProperty inputChannel;
    private final IntegerProperty outputChannel;

    public String getName() {
        return name.getValue();
    }

    public int getInputChannel() {
        return inputChannel.getValue();
    }

    public int getOutputChannel() {
        return outputChannel.getValue();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public IntegerProperty inputChannelProperty() {
        return inputChannel;
    }

    public IntegerProperty outputChannelProperty() {
        return outputChannel;
    }
}
