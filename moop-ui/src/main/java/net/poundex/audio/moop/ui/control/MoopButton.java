package net.poundex.audio.moop.ui.control;

import javafx.scene.control.Button;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.ui.util.AnimationController;

public abstract class MoopButton extends Button {

    protected final EngineContext engineContext;
    protected final AnimationController animationController;

    public MoopButton(String text, EngineContext engineContext, AnimationController animationController) {
        super(text);
        this.engineContext = engineContext;
        this.animationController = animationController;
        setFocusTraversable(false);
        getStyleClass().add("moop-button");
    }
}
