package net.poundex.audio.moop.ui.model;

import javafx.application.Platform;
import javafx.beans.value.WritableValue;

public interface Model {
    default <T> void setPropertyValueLater(WritableValue<T> property, T value) {
        Platform.runLater(() -> property.setValue(value));
    }
}
