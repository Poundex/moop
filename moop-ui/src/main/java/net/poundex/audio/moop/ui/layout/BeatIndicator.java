package net.poundex.audio.moop.ui.layout;

import javafx.application.Platform;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;
import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.messaging.listener.LoopTimeListener;
import net.poundex.audio.moop.ui.UiContext;
import net.poundex.audio.moop.ui.util.Colours;

import java.util.stream.IntStream;

import static net.poundex.audio.moop.ui.util.Colours.BEAT_INDICATOR_BEAT_OFF;

public class BeatIndicator extends HBox implements LoopTimeListener {
    private final EngineContext engineContext;
    private int beatsPerBar;
    private Beat[] beats;

    public BeatIndicator(EngineContext engineContext, int beatsPerBar) {
        this.engineContext = engineContext;
        this.beatsPerBar = beatsPerBar;
        relayout();
//        engineContext.getMessageBus().addLoopTimeListener(this);
        UiContext.INSTANCE.addAsyncLoopTimeListener(this);
    }

    private void relayout() {
        beats = new Beat[beatsPerBar];
        getChildren().clear();
        IntStream.range(0, beatsPerBar).forEach(beatIdx -> {
            beats[beatIdx] = new Beat(beatIdx == 0);
            getChildren().add(beats[beatIdx]);
        });
    }

    private int lastBeat = -1;
    @Override
    public void onLoopTime(int tickInLoop, boolean newLoop, int barInLoop, int beatInBar, int tickInBeat, int range) {
        if(beatInBar == lastBeat)
            return;
        lastBeat = beatInBar;
        for (int i = 0; i < beats.length; i++) {
            if (beatInBar == i)
                beats[i].on();
            else
                beats[i].off();
        }
    }

    @Slf4j
    private static class Beat extends Circle {

        private final boolean accent;

        private Beat(boolean accent) {
            super(10, BEAT_INDICATOR_BEAT_OFF);
            this.accent = accent;
        }

        private void on() {
            this.setFill(accent ? Colours.BEAT_INDICATOR_BEAT_ACCENT : Colours.BEAT_INDICATOR_BEAT_ON);
        }

        private void off() {
            this.setFill(BEAT_INDICATOR_BEAT_OFF);
        }
    }

}
