package net.poundex.audio.moop.ui.layout;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import net.poundex.audio.moop.ui.model.service.PartModelService;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.ui.util.AnimationController;

import java.util.concurrent.atomic.AtomicInteger;

public class LoopPanel extends GridPane {

    private final EngineContext engineContext;
    private final PartModelService partModelService;
    private final AnimationController animationController;

    public LoopPanel(EngineContext engineContext, PartModelService partModelService, AnimationController animationController) {
        this.engineContext = engineContext;
        this.partModelService = partModelService;
        this.animationController = animationController;
        setPadding(new Insets(2.0, 5.0, 1.0, 5.0));

        add(new Label(" "), 0, 0);
        add(new Button("A"), 1, 0);

        SimpleBooleanProperty sbp = new SimpleBooleanProperty(false);
        Timeline t = new Timeline(
                new KeyFrame(Duration.millis(300), new KeyValue(sbp, true)),
                new KeyFrame(Duration.millis(600), new KeyValue(sbp, false)));
        t.setCycleCount(Animation.INDEFINITE);
        t.playFromStart();

        AtomicInteger rows = new AtomicInteger(1);
        engineContext.getLooper().getParts().forEach(part -> {
                    add(new PartSettingsControl(partModelService.getPartModel(part), this.engineContext, this.animationController), 0, rows.get());
                    Label l = new Label("\u25A3\u25A3\u25A3\u25A3");
                    l.getStyleClass().add("tmpl");
                    add(l, 1, rows.get());
                    sbp.addListener((observable, oldValue, newValue) -> l.setTextFill(newValue ? Color.GREY : Color.RED));
                    rows.getAndIncrement();
                });

    }
}
