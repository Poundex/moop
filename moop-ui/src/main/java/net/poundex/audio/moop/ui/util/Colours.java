package net.poundex.audio.moop.ui.util;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Colours {
    public final static Paint BEAT_INDICATOR_BEAT_OFF = Color.BLACK;
    public final static Paint BEAT_INDICATOR_BEAT_ON = Color.RED;
    public final static Paint BEAT_INDICATOR_BEAT_ACCENT = Color.GREEN;
}
