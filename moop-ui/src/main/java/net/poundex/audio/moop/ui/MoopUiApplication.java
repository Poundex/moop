package net.poundex.audio.moop.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import net.poundex.audio.moop.core.exception.InitialisationException;
import net.poundex.audio.moop.ui.layout.ControlPanel;
import net.poundex.audio.moop.ui.layout.LoopPanel;

public class MoopUiApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        BorderPane root = new BorderPane();

        ControlPanel controlPanel = new ControlPanel(UiContext.INSTANCE.getEngineContext());
        root.setTop(controlPanel);

        LoopPanel loopPanel = new LoopPanel(
                UiContext.INSTANCE.getEngineContext(),
                UiContext.INSTANCE.getPartModelService(),
                UiContext.INSTANCE.getAnimationController());
        root.setCenter(loopPanel);

        Scene scene = new Scene(root);
        scene.getStylesheets().add("moop.css");

        startEngine();
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void startEngine() {
        try {
            UiContext.INSTANCE.getEngineContext().start();
            UiContext.INSTANCE.getEngineContext().getMessageBus().addLoopTimeListener(
                    UiContext.INSTANCE);
        } catch (InitialisationException iex) {
            new Alert(Alert.AlertType.ERROR, iex.getUserMessage()).showAndWait();
            Platform.exit();
        }
    }
}
