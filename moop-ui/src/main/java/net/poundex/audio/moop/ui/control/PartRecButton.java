package net.poundex.audio.moop.ui.control;

import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.CuePartRecCommand;
import net.poundex.audio.moop.core.messaging.listener.PartRecordCueListener;
import net.poundex.audio.moop.core.messaging.listener.PartRecordFinishListener;
import net.poundex.audio.moop.core.messaging.listener.PartRecordStartListener;
import net.poundex.audio.moop.ui.model.PartModel;
import net.poundex.audio.moop.ui.util.AnimationController;

public class PartRecButton extends MoopButton implements PartRecordCueListener, PartRecordStartListener, PartRecordFinishListener {

    private final PartModel partModel;

    public PartRecButton(EngineContext engineContext, PartModel partModel, AnimationController animationController) {
        super("REC", engineContext, animationController);
        this.partModel = partModel;
        getStyleClass().add("part-record-button");
        setOnAction(ae -> engineContext.sendCommand(new CuePartRecCommand(partModel.getId())));
        engineContext.getMessageBus().addPartRecordCueListener(this);
        engineContext.getMessageBus().addPartRecordStartListener(this);
        engineContext.getMessageBus().addPartRecordFinishListener(this);
    }

    @Override
    public void onPartRecordCue(int partId) {
        if(partId != partModel.getId())
            return;

        animationController.bindToCueAnimation(this,
                () -> getStyleClass().add("part-record-button-active-cue"),
                () -> getStyleClass().remove("part-record-button-active-cue"));
    }

    @Override
    public void onPartRecordStart(int partId) {
        if(partId != partModel.getId())
            return;

        animationController.unbindFromCueAnimation(this);
        getStyleClass().remove("part-record-button-active-cue");
        getStyleClass().add("part-record-button-active");
    }

    @Override
    public void onPartRecordFinish(int partId) {
        if(partId != partModel.getId())
            return;

        getStyleClass().remove("part-record-button-active");
    }
}
