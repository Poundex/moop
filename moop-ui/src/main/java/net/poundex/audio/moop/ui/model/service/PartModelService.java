package net.poundex.audio.moop.ui.model.service;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.RequiredArgsConstructor;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.looper.Part;
import net.poundex.audio.moop.core.messaging.listener.PartRenamedListener;
import net.poundex.audio.moop.ui.model.Model;
import net.poundex.audio.moop.ui.model.PartModel;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class PartModelService implements Model {
    private final EngineContext engineContext;
    private final Map<Part, PartModel> partModelsByPart = new HashMap<>(16);

    public PartModel getPartModel(Part part) {
        if( ! partModelsByPart.containsKey(part))
            partModelsByPart.put(part, createPartModel(part));

        return partModelsByPart.get(part);
    }

    private PartModel createPartModel(Part part) {
        StringProperty nameProperty = new SimpleStringProperty(part.getName());
        IntegerProperty inputChannelProperty = new SimpleIntegerProperty(part.getInputChannel());
        IntegerProperty outputChannelProperty = new SimpleIntegerProperty(part.getOutputChannel());
        engineContext.getMessageBus().addPartRenamedListener(new PartListener(part.getId(), nameProperty, inputChannelProperty, outputChannelProperty));
        return new PartModel(part.getId(), nameProperty, inputChannelProperty, outputChannelProperty);
    }

    @RequiredArgsConstructor
    private class PartListener implements PartRenamedListener {
        private final int partId;
        private final StringProperty nameProperty;
        private final IntegerProperty inputChannelProperty;
        private final IntegerProperty outputChannelProperty;

        public void onPartRenamed(int partId, String newName) {
            if(partId != this.partId)
                return;

//            nameProperty.setValue(e.getNewName());
            setPropertyValueLater(nameProperty, newName);
        }
    }
}
