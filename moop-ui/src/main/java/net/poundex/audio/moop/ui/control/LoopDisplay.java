package net.poundex.audio.moop.ui.control;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.messaging.listener.LoopTimeListener;

@Slf4j
public class LoopDisplay extends HBox implements LoopTimeListener {

    private int loopLength = 4; // TODO
    private Bar[] bars;

    public LoopDisplay() {
        relayout();
    }

    private void relayout() {
        Bar[] tmp = new Bar[loopLength];
        for (int i = 0; i < 4; i++) {
            tmp[i] = new Bar();
        }
        getChildren().setAll(tmp);
        bars = tmp;
    }

    @Override
    public void onLoopTime(int tickInLoop, boolean newLoop, int barInLoop, int beatInBar, int tickInBeat, int range) {
        if (beatInBar != 0)
            return;
        for (int i = 0; i < bars.length; i++) {
            if (barInLoop == i)
                bars[i].on();
            else
                bars[i].off();
        }
    }

    private class Bar extends Label {
        public Bar() {
            super("\u25A3");
            getStyleClass().add("loop-display-bar");
        }

        public void on() {
            getStyleClass().add("loop-display-bar-active");
        }

        public void off() {
            getStyleClass().remove("loop-display-bar-active");
        }
    }
}
