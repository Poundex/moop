package net.poundex.audio.moop.ui.util;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.util.Duration;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.Meter;
import net.poundex.audio.moop.core.messaging.listener.ClockStartListener;
import net.poundex.audio.moop.core.messaging.listener.MeterChangedListenenr;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AnimationController implements ClockStartListener, MeterChangedListenenr {

    private final EngineContext engineContext;
    private final BooleanProperty cueAnimationTicker = new SimpleBooleanProperty();
    private final Map<Object, ChangeListener<Boolean>>  cueAnimationTargets = new HashMap<>();

    private Meter meter;
    private Timeline cueAnimation;

    public AnimationController(EngineContext engineContext) {
        this.engineContext = engineContext;
        engineContext.getMessageBus().addClockStartListener(this);
        engineContext.getMessageBus().addMeterChangedListener(this);
    }

    public void bindToCueAnimation(Object target, Runnable showCueState, Runnable showCurrentState) {
        if(cueAnimationTargets.containsKey(target))
            return;

        ChangeListener<Boolean> listener = (observable, oldValue, newValue) -> {
            if(newValue)
                showCueState.run();
            else
                showCurrentState.run();
        };

        cueAnimationTicker.addListener(listener);
        cueAnimationTargets.put(target, listener);
    }

    @Override
    public void onClockStart() {
        cueAnimation.jumpTo(Duration.ZERO);
    }

    @Override
    public void onMeterChanged(Meter meter) {
        this.meter = meter;
        afterMeterUpdated();
    }

    private void afterMeterUpdated() {
        if(cueAnimation != null)
            cueAnimation.stop();

        cueAnimation = new Timeline(
                new KeyFrame(Duration.seconds(0), new KeyValue(cueAnimationTicker, true)),
                new KeyFrame(Duration.seconds(meter.getPulsePeriod() * 12),
                        new KeyValue(cueAnimationTicker, false)),
                new KeyFrame(Duration.seconds(meter.getPulsePeriod() * 24),
                        new KeyValue(cueAnimationTicker, true)));
        cueAnimation.setCycleCount(Animation.INDEFINITE);
        cueAnimation.play();
    }

    public void unbindFromCueAnimation(Object target) {
        Optional.ofNullable(cueAnimationTargets.get(target))
                .ifPresent(listener -> {
                    cueAnimationTicker.removeListener(listener);
                    cueAnimationTargets.remove(target);
                });
    }
}
