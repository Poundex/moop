package net.poundex.audio.moop.ui.layout;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.ChangeMidiInChannelForPartCommand;
import net.poundex.audio.moop.core.command.ChangeMidiOutChannelForPartCommand;
import net.poundex.audio.moop.core.command.RenamePartCommand;
import net.poundex.audio.moop.ui.control.MidiChannelSelector;
import net.poundex.audio.moop.ui.control.PartRecButton;
import net.poundex.audio.moop.ui.model.PartModel;
import net.poundex.audio.moop.ui.util.AnimationController;

public class PartSettingsControl extends BorderPane {

    private final EngineContext engineContext;
    private final PartModel partModel;
    private final AnimationController animationController;

    public PartSettingsControl(PartModel partModel, EngineContext engineContext, AnimationController animationController) {
        this.engineContext = engineContext;
        this.partModel = partModel;
        this.animationController = animationController;
        initLayout();
    }

    private void initLayout() {
        Button nameButton = new Button();
        nameButton.textProperty().bind(partModel.nameProperty());

        nameButton.setOnAction(event -> {
            TextInputDialog d = new TextInputDialog(partModel.getName());
            d.setHeaderText(String.format("New name for '%s':", partModel.getName()));
            d.showAndWait().ifPresent(newName ->
                    engineContext.sendCommand(new RenamePartCommand(partModel.getId(), newName)));
        });

        MidiChannelSelector inChannelSelector = MidiChannelSelector.in(partModel.inputChannelProperty(),
                newChannel -> engineContext.sendCommand(
                        new ChangeMidiInChannelForPartCommand(partModel.getId(), newChannel)));

        MidiChannelSelector outChannelSelector = MidiChannelSelector.out(partModel.outputChannelProperty(),
                newChannel -> engineContext.sendCommand(
                        new ChangeMidiOutChannelForPartCommand(partModel.getId(), newChannel)));

        setLeft(new PartRecButton(engineContext, partModel, animationController));
        setCenter(new VBox(
                new BorderPane(nameButton, null, new Button("G"), null, null),
                new HBox(new Button("M"), new Button("S"),
                        new VBox(new HBox(new Label("I:"), inChannelSelector), new HBox(new Label("O:"), outChannelSelector)))));
    }

}
