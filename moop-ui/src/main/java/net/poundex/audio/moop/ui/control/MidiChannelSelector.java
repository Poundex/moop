package net.poundex.audio.moop.ui.control;

import javafx.beans.property.IntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.Consumer;

@Slf4j
public class MidiChannelSelector extends ChoiceBox<Integer> {

    private static final ObservableList<Integer> IN_CHANNELS = FXCollections.observableList(List.of(-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15));
    private static final ObservableList<Integer> OUT_CHANNELS = FXCollections.observableList(List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15));

    private MidiChannelSelector(ObservableList<Integer> items, IntegerProperty channelProperty, ChangeListener<? super Integer> changeListener) {
        super(items);
        setConverter(new StringConverter<>() {
            @Override
            public String toString(Integer object) {
                if (object == -1)
                    return "All";
                return "" + (object + 1);
            }

            @Override
            public Integer fromString(String string) {
                if (string.equals("All"))
                    return -1;
                return Integer.parseInt(string) - 1;
            }
        });
        setValue(channelProperty.getValue());
        getSelectionModel().selectedItemProperty().addListener(changeListener);
    }

    public static MidiChannelSelector in(IntegerProperty channelProperty, Consumer<Integer> onChannelChanged) {
        return new MidiChannelSelector(IN_CHANNELS, channelProperty, adaptListener(onChannelChanged));
    }

    public static MidiChannelSelector out(IntegerProperty channelProperty, Consumer<Integer> onChannelChanged) {
        return new MidiChannelSelector(OUT_CHANNELS, channelProperty, adaptListener(onChannelChanged));
    }

    private static ChangeListener<? super Integer> adaptListener(Consumer<Integer> onChannelChanged) {
        return (ChangeListener<Integer>) (observable, oldValue, newValue) ->
                onChannelChanged.accept(newValue);
    }
}
