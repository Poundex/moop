package net.poundex.audio.moop.jack;

import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.AudioSystem;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.Meter;
import net.poundex.audio.moop.core.exception.AudioSystemInitialisationException;
import net.poundex.audio.moop.core.messaging.listener.MeterChangedListenenr;
import net.poundex.audio.moop.core.util.MidiWire;
import net.poundex.audio.moop.engine.util.ObjectPool;
import net.poundex.audio.moop.engine.util.ObjectPool.QueuedMidiEvent;
import org.jaudiolibs.jnajack.*;

import java.util.*;

@Slf4j
public class JackAudioSystem implements AudioSystem, JackProcessCallback, MeterChangedListenenr {

    private Jack jack;
    private JackClient client;
    private JackPort midiInPort;
    private JackPort midiOutPort;
    private JackPort clickOutPort;

    private EngineContext engineContext;
    private Meter meter;
    private final JackMidi.Event midiEventBuffer = new JackMidi.Event();
    private final byte[] midiDataBuffer = new byte[4];
    private final Queue<QueuedMidiEvent> midiPlaybackQueue = new PriorityQueue<>(
            Comparator.comparingDouble(QueuedMidiEvent::getTickOffset));
    private int clkEvCount;
    private int midiInPortPendingEventCount;
    private int lastClkEvSize;
    private int lastMidiInEventSize;
    private byte[] eventToSend;
    private int bufferSize;

    private double tickLengthInNanos;
    private final MidiClockGenerator midiClockGenerator = new MidiClockGenerator(this);
    private double bufferTimeInNanos;
    private double bufferTimeInTicks;
    private double framesPerTick;

    private boolean Z_rolling = false;
    private int eventSendRelativeOrder = 0;

    @Override
    public void start(EngineContext engineContext) throws AudioSystemInitialisationException {
        this.engineContext = engineContext;

        try {
            engineContext.getMessageBus().addMeterChangedListener(this);
            engineContext.getMessageBus().addLoopTimeListener(midiClockGenerator);
            doStart();
        } catch (JackException jex) {
            throw new AudioSystemInitialisationException("Jack", jex);
        }
    }

    @Override
    public void stop() {
        client.deactivate();
    }

    @Override
    public void transportStart() {
//        try {
//            client.transportStart();
//        } catch (JackException e) {
//            log.error("transportStart", e); // TODO
//        }
//        originTime = -1L;
//        midiClockGenerator.transportStart();
        Z_rolling = true;
    }

    @Override
    public void transportStop() {
//        try {
//            client.transportStop();
//            client.transportLocate(0);
//        } catch (JackException e) {
//            log.error("transportStop", e); // TODO
//        }
//        midiClockGenerator.transportStop();
    }

    private void doStart() throws JackException {
        jack = Jack.getInstance();

        client = jack.openClient("Moop", EnumSet.of(JackOptions.JackNoStartServer),
                EnumSet.noneOf(JackStatus.class));

        bufferSize = client.getBufferSize();

        midiInPort = client.registerPort("MIDI In", JackPortType.MIDI,
                EnumSet.of(JackPortFlags.JackPortIsInput));
        midiOutPort = client.registerPort("MIDI Out", JackPortType.MIDI,
                EnumSet.of(JackPortFlags.JackPortIsOutput));

        clickOutPort = client.registerPort("Click Out", JackPortType.MIDI,
                JackPortFlags.JackPortIsOutput);

        client.setProcessCallback(this);
        client.activate();
    }

    private void calculateTimings() {
        tickLengthInNanos = ((1 / ((meter.getTempo() / 60.0) * Meter.TICKS_PER_BEAT)) * 1e9);
        bufferTimeInNanos = ((1024.0 / 48_000.0) * 1e9);
        bufferTimeInTicks = (bufferTimeInNanos / tickLengthInNanos);
        framesPerTick = 1024.0 / bufferTimeInTicks;
    }

    @Override
    public void onMeterChanged(Meter meter) {
        this.meter = meter;
        calculateTimings();
    }

    @Override
    public boolean process(JackClient client, int nframes) {
        try {
            return doProcess(client, nframes);
        } catch (Exception jex) {
            log.error("In process", jex);
        }
        return true; // TODO
    }

    private List<Long> samples = new ArrayList<>(10_000);
    private void addSample(long duration) {
        samples.add(duration);
        if(samples.size() == 3_000) {
            double avg = samples.stream().skip(1).mapToLong(l -> l).average().getAsDouble();
            long max = samples.stream().skip(1).mapToLong(l -> l).max().getAsLong();
            long min = samples.stream().skip(1).mapToLong(l -> l).min().getAsLong();
            log.info("avg: {}, max: {}, min: {}", avg, max, min);
            log.info("{}", samples);
            System.exit(0);
        }
    }

    private long originTime = -1L;

    private boolean doProcess(JackClient client, int nframes) throws JackException {
//        long Z_processStart = System.nanoTime();

        if( ! Z_rolling) return true;

        JackMidi.clearBuffer(midiOutPort);

        long startFrame = client.getLastFrameTime();
        long absoluteJackTime = (long) (startFrame / 48_000.0 * 1e9);
        if(originTime == -1L) originTime = absoluteJackTime;

        long timeSinceOrigin = absoluteJackTime - originTime;
        int ticksSinceOrigin = (int) (timeSinceOrigin / tickLengthInNanos);
        double absoluteFrameOfLastTickStart = (ticksSinceOrigin * framesPerTick);
        int nextTickStartFrame = (int) ((absoluteFrameOfLastTickStart + framesPerTick) % nframes);
        int remainingTickStartsInBufferAfterOffset = (int)
                ((nframes - nextTickStartFrame) / framesPerTick);

        engineContext.getMessageBus().fireGlobalTime(
                ticksSinceOrigin, remainingTickStartsInBufferAfterOffset);

        eventSendRelativeOrder = 0;

        midiInPortPendingEventCount = JackMidi.getEventCount(midiInPort);
        for (int i = 0; i < midiInPortPendingEventCount; i++) {
            JackMidi.eventGet(midiEventBuffer, midiInPort, i);
            lastMidiInEventSize = midiEventBuffer.size();
            midiEventBuffer.read(midiDataBuffer);

            double frameOffsetInTicks = ((midiEventBuffer.time() / framesPerTick));
            int absoluteTickForThisEvent =
                    (int) Math.ceil (((ticksSinceOrigin + frameOffsetInTicks) - bufferTimeInTicks));
            engineContext.getMessageBus().fireMidiDataEvent(
                    absoluteTickForThisEvent, midiDataBuffer, lastMidiInEventSize);
        }

        QueuedMidiEvent event;
        while((event = midiPlaybackQueue.poll()) != null) {
            int tickOffsetInFrames = (int) ((((int)event.getTickOffset()) * framesPerTick) + nextTickStartFrame);
            JackMidi.eventWrite(midiOutPort, tickOffsetInFrames, event.getData(), event.getData().length);
            ObjectPool.midiEvents.offer(event);
        }
//        addSample(System.nanoTime() - Z_processStart);
        return true;
    }

    @Override
    public void playMidi(byte[] data, double tickOffset) {
        midiPlaybackQueue.add(
                ObjectPool.midiEvents.remove().rehydrate(tickOffset, data));
    }

}
