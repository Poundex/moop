package net.poundex.audio.moop.jack;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.AudioSystem;
import net.poundex.audio.moop.core.Meter;
import net.poundex.audio.moop.core.messaging.listener.LoopTimeListener;
import net.poundex.audio.moop.core.util.MidiWire;

@RequiredArgsConstructor
@Slf4j
public class MidiClockGenerator implements LoopTimeListener {
    private final static int PULSES_PER_BEAT = 24;
    private final static int TICKS_PER_PULSE =
            Meter.TICKS_PER_BEAT / PULSES_PER_BEAT;
    private final static byte[] CLOCK_PULSE_EVENT = new byte[] { MidiWire.STATUS_CLOCK_PULSE };
    private final static byte[] CLOCK_START_EVENT = new byte[] { MidiWire.STATUS_CLOCK_START };
    private final static byte[] CLOCK_STOP_EVENT = new byte[] { MidiWire.STATUS_CLOCK_STOP };
    private final static byte[] PLAY_CLICK_EVENT = new byte[] { MidiWire.channelise(MidiWire.STATUS_NOTE_ON, (byte) 9), 33, 120 };
    private final static byte[] PLAY_CLICK_OFF_EVENT = new byte[] { MidiWire.channelise(MidiWire.STATUS_NOTE_OFF, (byte) 9), 33, 0 };
    private final static byte[] PLAY_CLICK_ACCENT_EVENT = new byte[] { MidiWire.channelise(MidiWire.STATUS_NOTE_ON, (byte) 9), 34, 120 };
    private final static byte[] PLAY_CLICK_ACCENT_OFF_EVENT = new byte[] { MidiWire.channelise(MidiWire.STATUS_NOTE_OFF, (byte) 9), 34, 0 };
    private static final int TICKS_PER_SEMIQUAVER = Meter.TICKS_PER_BEAT / 2 / 2;

    private final AudioSystem audioSystem;

    private boolean rolling = false;
    private boolean startNeverSent = true;

    private int nextPulseTick = -1;
    private boolean startFromStopped;
    private boolean stopRequested;

    @Override
    public void onLoopTime(int tickInLoop, boolean newLoop, int barInLoop, int beatInBar, int tickInBeat, int range) {

        if (nextPulseTick == -1)
            if (tickInBeat % TICKS_PER_PULSE == 0)
                nextPulseTick = tickInBeat;
            else
                nextPulseTick = (tickInBeat / TICKS_PER_PULSE * TICKS_PER_PULSE + TICKS_PER_PULSE) % Meter.TICKS_PER_BEAT;

        while (nextPulseTick >= tickInBeat && nextPulseTick <= tickInBeat + range
                || (tickInBeat + range) / Meter.TICKS_PER_BEAT > nextPulseTick) {

            int tickOffset = (nextPulseTick - tickInBeat + Meter.TICKS_PER_BEAT) % Meter.TICKS_PER_BEAT;
            if (startNeverSent) {
                audioSystem.playMidi(CLOCK_START_EVENT, tickOffset);
                startNeverSent = false;
            }
            audioSystem.playMidi(CLOCK_PULSE_EVENT, tickOffset);

            int barOffset = (tickInLoop + tickOffset) % (Meter.TICKS_PER_BEAT * 4) /* TODO */;
            if (barOffset == 0 && nextPulseTick % Meter.TICKS_PER_BEAT == 0) {
                audioSystem.playMidi(PLAY_CLICK_ACCENT_EVENT, tickOffset);
                audioSystem.playMidi(PLAY_CLICK_EVENT, tickOffset);
            }
            else if(nextPulseTick % Meter.TICKS_PER_BEAT == 0) {
                audioSystem.playMidi(PLAY_CLICK_EVENT, tickOffset);
            }
            else if (nextPulseTick % Meter.TICKS_PER_BEAT == TICKS_PER_SEMIQUAVER) {
                audioSystem.playMidi(PLAY_CLICK_OFF_EVENT, tickOffset);
                audioSystem.playMidi(PLAY_CLICK_ACCENT_OFF_EVENT, tickOffset);
            }

            nextPulseTick = (nextPulseTick + TICKS_PER_PULSE) % Meter.TICKS_PER_BEAT;
        }
    }

    public void transportStart() {
        if(rolling) return;
        startFromStopped = true;
//        nextPulseTick = -1;
        rolling = true;
    }

    public void transportStop() {
        stopRequested = true;
        rolling = false;
    }
}
