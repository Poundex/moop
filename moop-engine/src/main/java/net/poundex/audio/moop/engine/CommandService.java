package net.poundex.audio.moop.engine;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.Command;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.*;
import net.poundex.audio.moop.engine.action.*;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@RequiredArgsConstructor
public class CommandService {
    
    private final static Map<Class<? extends Command>, Action<?>> commandToAction =
            Map.of(TransportStartCommand.class, TransportStartAction.INSTANCE,
                    TransportStopCommand.class, TransportStopAction.INSTANCE,
                    RenamePartCommand.class, RenamePartAction.INSTANCE,
                    CuePartRecCommand.class, CuePartRecordAction.INSTANCE,
                    SetMeterCommand.class, SetMeterAction.INSTANCE,
                    ChangeMidiInChannelForPartCommand.class, ChangeMidiInChannelForPartAction.INSTANCE,
                    ChangeMidiOutChannelForPartCommand.class, ChangeMidiOutChannelForPartAction.INSTANCE
            );

    private static int commandSenderThreadCounter = 0;

    private final EngineContext engineContext;
    private final ExecutorService commandExecutor = Executors.newFixedThreadPool(4, r -> {
        Thread t = new Thread(r, "command-sender-" + commandSenderThreadCounter++);
        t.setUncaughtExceptionHandler((t1, e) -> log.error("in command sender", e));
        return t;
    });


    public <T extends Command> void runCommand(T command) {
        Optional.ofNullable(commandToAction.get(command.getClass())).ifPresentOrElse(
                action -> commandExecutor.submit(() ->
                        ((Action<T>) action).accept(engineContext, command)),
                () -> { throw new RuntimeException("FIXME: NO ACTION FOR COMMAND " + command.getClass().getSimpleName()); });
    }
}
