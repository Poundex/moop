package net.poundex.audio.moop.engine;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.*;
import net.poundex.audio.moop.core.command.SetMeterCommand;
import net.poundex.audio.moop.core.exception.InitialisationException;
import net.poundex.audio.moop.core.messaging.listener.LoopTimeListener;
import net.poundex.audio.moop.core.messaging.listener.MidiPerformanceEventListener;

@RequiredArgsConstructor
@Slf4j
public class MoopEngine implements EngineContext {
    @Getter
    private final WritableMessageBus messageBus;
    @Getter
    private final AudioSystem audioSystem;
    private final CommandService commandService;
    @Getter
    private final LooperService looper;
    private final MidiProcessorService midiProcessorService;

    public MoopEngine(AudioSystem audioSystem) {
        this.messageBus = new DefaultMessageBus();
        this.audioSystem = audioSystem;
        this.midiProcessorService = new MidiProcessorService(messageBus);
        this.commandService = new CommandService(this);
        this.looper = new LooperService(this);
    }

    @Override
    public void start() throws InitialisationException {
        audioSystem.start(this);
        looper.getParts().forEach(p -> {
            // TODO !! !! !!
            messageBus.addMidiPerformanceEventListener((MidiPerformanceEventListener) p);
            messageBus.addLoopTimeListener((LoopTimeListener) p);
        });
        commandService.runCommand(new SetMeterCommand(new Meter(120.0, 4, 4)));
    }

    @Override
    public void stop() {
        audioSystem.stop();
    }

    @Override
    public void sendCommand(Command command) {
        commandService.runCommand(command);
    }
}
