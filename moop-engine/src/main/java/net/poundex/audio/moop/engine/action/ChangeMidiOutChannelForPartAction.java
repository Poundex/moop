package net.poundex.audio.moop.engine.action;

import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.ChangeMidiInChannelForPartCommand;
import net.poundex.audio.moop.engine.Action;

public enum ChangeMidiOutChannelForPartAction implements Action<ChangeMidiInChannelForPartCommand> {
    INSTANCE;

    @Override
    public void accept(EngineContext engineContext, ChangeMidiInChannelForPartCommand command) {
        engineContext.getLooper().getPart(command.getPartId()).setInputChannel(command.getNewChannel());
        engineContext.getMessageBus().fireMidiInChannelChanged(command.getPartId(), command.getNewChannel());
    }
}
