package net.poundex.audio.moop.engine;

import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.Meter;
import net.poundex.audio.moop.core.looper.Looper;
import net.poundex.audio.moop.core.looper.Part;
import net.poundex.audio.moop.core.messaging.listener.GlobalTimeListener;
import net.poundex.audio.moop.core.messaging.listener.LoopLengthChangedListener;
import net.poundex.audio.moop.core.messaging.listener.MeterChangedListenenr;
import net.poundex.audio.moop.core.messaging.listener.MidiPerformanceEventListener;
import net.poundex.audio.moop.engine.looper.DefaultPart;
import net.poundex.audio.moop.engine.looper.EventProcessor;
import net.poundex.audio.moop.engine.util.ObjectPool;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Slf4j
public class LooperService implements Looper, GlobalTimeListener, MeterChangedListenenr, LoopLengthChangedListener, MidiPerformanceEventListener {

    private final EngineContext engineContext;
    private final Map<Integer, DefaultPart> parts = new LinkedHashMap<>(16);
    private final EventProcessor eventProcessor;

    private Meter meter;
    private int loopLengthInTicks = 1920 * 4 * 4; // TODO

    public LooperService(EngineContext engineContext) {
        this.engineContext = engineContext;
        engineContext.getMessageBus().addGlobalTimeListener(this);
        engineContext.getMessageBus().addMeterChangedListener(this);
        engineContext.getMessageBus().addLoopLengthChangedListener(this);
        engineContext.getMessageBus().addMidiPerformanceEventListener(this);
        Z_generateParts(); // TODO
        this.eventProcessor = new EventProcessor(parts);
        engineContext.getMessageBus().addPartRecordCueListener(this.eventProcessor);
        eventProcessor.start();
    }

    private void Z_generateParts() {
        Stream.of(
                new DefaultPart(engineContext.getMessageBus(), engineContext.getAudioSystem(), "Bass", 30720, -1, 0),
                new DefaultPart(engineContext.getMessageBus(), engineContext.getAudioSystem(), "Chord 1", 30720, -1, 1),
                new DefaultPart(engineContext.getMessageBus(), engineContext.getAudioSystem(), "Phrase 1", 30720, -1, 2),
                new DefaultPart(engineContext.getMessageBus(), engineContext.getAudioSystem(), "Rhythm 1", 30720, -1, 9),
                new DefaultPart(engineContext.getMessageBus(), engineContext.getAudioSystem(), "Pad", 30720, -1, 3),
                new DefaultPart(engineContext.getMessageBus(), engineContext.getAudioSystem(), "Chord 2", 30720, -1, 4),
                new DefaultPart(engineContext.getMessageBus(), engineContext.getAudioSystem(), "Phrase 2", 30720, -1, 5),
                new DefaultPart(engineContext.getMessageBus(), engineContext.getAudioSystem(),"Rhythm 2", 30720, -1, 9)
        ).forEach(p -> parts.put(p.getId(), p));
    }

    @Override
    public List<Part> getParts() {
        return List.copyOf(parts.values());
    }

    @Override
    public Part getPart(int partId) {
        return parts.get(partId);
    }

    private long lastTickInLoop = Integer.MAX_VALUE;
    @Override
    public void onGlobalTimeEvent(long startTick, int range) {
        int tickInLoop = (int) (startTick % loopLengthInTicks);
        int barInLoop = tickInLoop / meter.getTicksPerBar();
        int beatInBar = (tickInLoop % meter.getTicksPerBar()) / Meter.TICKS_PER_BEAT;
        int tickInBeat = tickInLoop % Meter.TICKS_PER_BEAT;

        boolean newLoop = tickInLoop < lastTickInLoop;
        lastTickInLoop = tickInLoop;
        engineContext.getMessageBus().fireLoopTime(tickInLoop, newLoop, barInLoop, beatInBar, tickInBeat, range);
    }

    @Override
    public void onLoopLengthChanged(int loopLength) {
        this.loopLengthInTicks = loopLength;
    }

    @Override
    public void onMeterChanged(Meter meter) {
        this.meter = meter;
    }

    @Override
    public void onMidiPerformanceEvent(int absoluteTick, int channel, byte[] data) {
        eventProcessor.queueEvent(
                ObjectPool.rawInboundEvent.remove()
                        .rehydrate(absoluteTick % loopLengthInTicks, channel, data));
    }
}
