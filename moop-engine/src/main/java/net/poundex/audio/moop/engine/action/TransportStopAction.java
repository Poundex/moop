package net.poundex.audio.moop.engine.action;

import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.TransportStopCommand;
import net.poundex.audio.moop.engine.Action;

public enum TransportStopAction implements Action<TransportStopCommand> {
    INSTANCE;

    @Override
    public void accept(EngineContext engineContext, TransportStopCommand command) {
        engineContext.getAudioSystem().transportStop();
    }
}
