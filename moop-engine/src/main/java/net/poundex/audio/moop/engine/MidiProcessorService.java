package net.poundex.audio.moop.engine;

import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.messaging.listener.MidiDataEventListener;
import net.poundex.audio.moop.core.util.MidiWire;

@Slf4j
public class MidiProcessorService implements MidiDataEventListener {

    private final static byte LAST_INTERESTING_UNNORMALISED_STATUS = MidiWire.channelise(MidiWire.STATUS_PITCH_BEND, (byte) 15);

    private final WritableMessageBus messageBus;

    public MidiProcessorService(WritableMessageBus messageBus) {
        this.messageBus = messageBus;
        messageBus.addMidiDataEventListener(this);
    }

    @Override
    public void onMidiDataEvent(int absoluteTick, byte[] data, int size) {
        if(data[0] > LAST_INTERESTING_UNNORMALISED_STATUS)
            return;

        byte[] newData = new byte[size];
        newData[0] = MidiWire.normalise(data[0]);

        System.arraycopy(data, 1, newData, 1, size - 1);
        messageBus.fireMidiPerformanceEvent(
                absoluteTick, MidiWire.channel(data[0]), newData);
    }
}
