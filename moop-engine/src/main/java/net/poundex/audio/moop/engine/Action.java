package net.poundex.audio.moop.engine;

import net.poundex.audio.moop.core.Command;
import net.poundex.audio.moop.core.EngineContext;

import java.util.function.BiConsumer;

public interface Action<T extends Command> extends BiConsumer<EngineContext, T> {
}
