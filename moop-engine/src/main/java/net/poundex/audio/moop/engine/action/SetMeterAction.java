package net.poundex.audio.moop.engine.action;

import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.SetMeterCommand;
import net.poundex.audio.moop.engine.Action;

public enum SetMeterAction implements Action<SetMeterCommand> {
    INSTANCE;

    @Override
    public void accept(EngineContext engineContext, SetMeterCommand command) {
        engineContext.getMessageBus().fireMeterChanged(command.getNewMeter());
    }
}
