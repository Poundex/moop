package net.poundex.audio.moop.engine;

import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.Meter;
import net.poundex.audio.moop.core.messaging.listener.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("ForLoopReplaceableByForEach")
@Slf4j
public class DefaultMessageBus implements WritableMessageBus {
    
    private final ExecutorService executor;

    public DefaultMessageBus() {
        this.executor = Executors.newFixedThreadPool(
                Math.max(1, Runtime.getRuntime().availableProcessors() - 1),
                DefaultMessageBus::createThread);
    }

    private static final AtomicInteger workerCount = new AtomicInteger(0);
    private static Thread createThread(Runnable runnable) {
        Thread t = new Thread(runnable, "message-bus-" + workerCount.getAndIncrement());
        t.setUncaughtExceptionHandler((t1, e) -> log.error("Error", e));
        return t;
    }

    private final List<GlobalTimeListener> globalTimeListeners = new ArrayList<>();

    @Override
    public void addGlobalTimeListener(GlobalTimeListener listener) {
        globalTimeListeners.add(listener);
//        this.globalTimeListener = listener;
    }

    @Override
    public void fireGlobalTime(long startTick, int range) {
        for (int i = 0; i < globalTimeListeners.size(); i++) {
            globalTimeListeners.get(i).onGlobalTimeEvent(startTick, range);
        }
    }

    private final List<LoopLengthChangedListener> loopLengthChangedListeners = new ArrayList<>();

    @Override
    public void addLoopLengthChangedListener(LoopLengthChangedListener listener) {
        loopLengthChangedListeners.add(listener);
    }

    @Override
    public void fireLoopLengthChanged(int loopLength) {
        loopLengthChangedListeners.forEach(l -> executor.submit(() ->
                l.onLoopLengthChanged(loopLength)));
    }

//    private final List<LoopTimeListener> loopTimeListeners = new ArrayList<>();
    private LoopTimeListener[] loopTimeListeners2 = new LoopTimeListener[0];
    private int numLTLs = 0;

    @Override
    public void addLoopTimeListener(LoopTimeListener listener) {
//        loopTimeListeners.add(listener);
        int reqd = numLTLs + 1;
        LoopTimeListener[] newls = new LoopTimeListener[reqd];
        System.arraycopy(loopTimeListeners2, 0, newls, 0, numLTLs);
        newls[reqd - 1] = listener;
        numLTLs = reqd;
        loopTimeListeners2 = newls;
    }

    @Override
    public void fireLoopTime(int tickInLoop, boolean newLoop, int barInLoop, int beatInBar, int tickInBeat, int range) {
//        loopTimeListeners.forEach(l -> executor.submit(() -> l.onLoopTime(pulseInLoop, newLoop, barInLoop, beatInBar, pulseInBeat)));
//        loopTimeListeners.forEach(l -> l.onLoopTime(pulseInLoop, newLoop, barInLoop, beatInBar, pulseInBeat));
        for (int i = 0; i < numLTLs; i++) {
            loopTimeListeners2[i].onLoopTime(tickInLoop, newLoop, barInLoop, beatInBar, tickInBeat, range);
        }

    }

    private final List<MeterChangedListenenr> meterChangedListenenrs = new ArrayList<>();

    @Override
    public void addMeterChangedListener(MeterChangedListenenr listener) {
        meterChangedListenenrs.add(listener);
    }

    @Override
    public void fireMeterChanged(Meter meter) {
        meterChangedListenenrs.forEach(l -> executor.submit(() ->
                l.onMeterChanged(meter)));
    }

//    private final List<MidiDataEventListener> midiDataEventListeners = new ArrayList<>();
    private MidiDataEventListener[] midiDataEventListeners2 = new MidiDataEventListener[0];
    private int numMidiDataEvLs = 0;

    @Override
    public void addMidiDataEventListener(MidiDataEventListener listener) {
//        midiDataEventListeners.add(listener);
        int reqd = numMidiDataEvLs + 1;
        MidiDataEventListener[] newls = new MidiDataEventListener[reqd];
        System.arraycopy(midiDataEventListeners2, 0, newls, 0, numMidiDataEvLs);
        newls[reqd - 1] = listener;
        numMidiDataEvLs = reqd;
        midiDataEventListeners2 = newls;
    }

    @Override
    public void fireMidiDataEvent(int absoluteTick, byte[] data, int size) {
//        midiDataEventListeners.forEach(l -> executor.submit(() ->
//                l.onMidiDataEvent(data)));
//        midiDataEventListeners.forEach(l -> l.onMidiDataEvent(data, size));
        for (int i = 0; i < numMidiDataEvLs; i++) {
            midiDataEventListeners2[i].onMidiDataEvent(absoluteTick, data, size);
        }
    }

    private final List<MidiPerformanceEventListener> midiPerformanceEventListeners = new ArrayList<>();

    @Override
    public void addMidiPerformanceEventListener(MidiPerformanceEventListener listener) {
        midiPerformanceEventListeners.add(listener);
    }

    @Override
    public void fireMidiPerformanceEvent(int absoluteTick, int channel, byte[] data) {
//        midiPerformanceEventListeners.forEach(l -> executor.submit(() ->
//                l.onMidiPerformanceEvent(loopTime, channel, data)));
        midiPerformanceEventListeners.forEach(l -> l.onMidiPerformanceEvent(absoluteTick, channel, data));
    }

    private final List<PartRecordCueListener> partRecordCueListeners = new ArrayList<>();

    @Override
    public void addPartRecordCueListener(PartRecordCueListener listener) {
        partRecordCueListeners.add(listener);
    }

    @Override
    public void firePartRecordCue(int partId) {
        partRecordCueListeners.forEach(l -> executor.submit(() -> l.onPartRecordCue(partId)));
    }

    private final List<PartRecordStartListener> partRecordStartListeners = new ArrayList<>();

    @Override
    public void addPartRecordStartListener(PartRecordStartListener listener) {
        partRecordStartListeners.add(listener);
    }

    @Override
    public void firePartRecordStart(int partId) {
        partRecordStartListeners.forEach(l -> executor.submit(() -> l.onPartRecordStart(partId)));
    }

    private final List<PartRecordFinishListener> partRecordFinishListeners = new ArrayList<>();

    @Override
    public void addPartRecordFinishListener(PartRecordFinishListener listener) {
        partRecordFinishListeners.add(listener);
    }

    @Override
    public void firePartRecordFinish(int partId) {
        partRecordFinishListeners.forEach(l -> executor.submit(() -> l.onPartRecordFinish(partId)));
    }

    private final List<ClockStartListener> clockStartListeners = new ArrayList<>();

    @Override
    public void addClockStartListener(ClockStartListener listener) {
        clockStartListeners.add(listener);
    }

    @Override
    public void fireClockStart() {
        clockStartListeners.forEach(l -> executor.submit(l::onClockStart));
    }

    private final List<ClockStopListener> clockStopListeners = new ArrayList<>();

    @Override
    public void addClockStopListener(ClockStopListener listener) {
        clockStopListeners.add(listener);
    }

    @Override
    public void fireClockStop() {
        clockStopListeners.forEach(l -> executor.submit(l::onClockStop));
    }

    private final List<PartRenamedListener> partRenamedListeners = new ArrayList<>();

    @Override
    public void addPartRenamedListener(PartRenamedListener listener) {
        partRenamedListeners.add(listener);
    }

    @Override
    public void firePartRenamed(int partId, String newName) {
        partRenamedListeners.forEach(l -> executor.submit(() ->
                l.onPartRenamed(partId, newName)));
    }

    @Override
    public void addMidiInChannelChangedListener(MidiInChannelChangedListener listener) {

    }

    @Override
    public void addMidiOutChannelChangedListener(MidiOutChannelChangedListener listener) {

    }

    @Override
    public void fireMidiInChannelChanged(int partId, int newChannel) {

    }

    @Override
    public void fireMidiOutChannelChanged(int partId, int newChannel) {

    }
}
