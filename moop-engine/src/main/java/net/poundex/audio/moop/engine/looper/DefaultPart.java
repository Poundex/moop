package net.poundex.audio.moop.engine.looper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.AudioSystem;
import net.poundex.audio.moop.core.Meter;
import net.poundex.audio.moop.core.looper.Part;
import net.poundex.audio.moop.core.messaging.listener.LoopTimeListener;
import net.poundex.audio.moop.core.messaging.listener.MidiInChannelChangedListener;
import net.poundex.audio.moop.core.messaging.listener.MidiOutChannelChangedListener;
import net.poundex.audio.moop.core.messaging.listener.MidiPerformanceEventListener;
import net.poundex.audio.moop.core.util.MidiWire;
import net.poundex.audio.moop.engine.WritableMessageBus;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Slf4j
public class DefaultPart implements Part, LoopTimeListener, MidiInChannelChangedListener, MidiOutChannelChangedListener, MidiPerformanceEventListener {
    private static final int TICKS_PER_SEMIQUAVER = Meter.TICKS_PER_BEAT / 2 / 2;

    private final static AtomicInteger partCounter = new AtomicInteger(0);

    private final WritableMessageBus messageBus;
    private final AudioSystem audioSystem;

    @EqualsAndHashCode.Include
    private final int id = partCounter.getAndIncrement();
    private final int length;

    private String name;
    private int inputChannel;
    private int outputChannel;
    private boolean cue;
    private boolean recording;

    private List<byte[]>[] preRecordBuffer;
    private List<byte[]>[] playbackBuffer;
    private List<byte[]>[] nextPlaybackBuffer;
    private boolean lastRecordRescued = false;

    public DefaultPart(WritableMessageBus messageBus, AudioSystem audioSystem, String name, int length, int inputChannel, int outputChannel) {
        this.messageBus = messageBus;
        this.audioSystem = audioSystem;
        this.name = name;
        this.length = length;
        this.inputChannel = inputChannel;
        this.outputChannel = outputChannel;
        this.playbackBuffer = createBuffer();
    }

    private List<byte[]>[] createBuffer() {
        List<byte[]>[] tmp = new List[length];
        for (int i = 0; i < length; i++) {
            tmp[i] = new ArrayList<>(4);
        }
        return tmp;
    }

    @Override
    public void setCue(boolean cue) {
        this.cue = cue;
        if(cue) prepareForRecord();
    }

    private void prepareForRecord() {
        this.preRecordBuffer = createBuffer();
        this.nextPlaybackBuffer = createBuffer();
    }

    @Override
    public void setRecording(boolean recording) {
        this.recording = recording;
    }

    private boolean isMyInputChannel(int channel) {
        return inputChannel == -1 || inputChannel == channel;
    }

    private long lastAbsTickForMidiEvent = -1;
    private int eventOrderCounter = 0 ;
    @Override
    public void onMidiPerformanceEvent(int absoluteTick, int channel, byte[] data) {
        if( ! isMyInputChannel(channel))
            return;

        if(lastAbsTickForMidiEvent != absoluteTick) {
            eventOrderCounter = 0;
            lastAbsTickForMidiEvent = absoluteTick;
        }

        if(recording | cue) {
            double tickOffset = 0 + (eventOrderCounter++ * 0.01);
            playMidi(data, tickOffset);
        }
    }

    private void playMidi(byte[] data, double tickOffset) {
        int length = data.length;
        byte[] play = new byte[length];
        play[0] = MidiWire.channelise(data[0], (byte) outputChannel /* TODO Unnecessary int */);
        System.arraycopy(data, 1, play, 1, length - 1);
        audioSystem.playMidi(play, tickOffset);
    }

    @Override
    public void onLoopTime(int tickInLoop, boolean newLoop, int barInLoop, int beatInBar, int tickInBeat, int range) {
        if (newLoop) {
            if (cue) {
                cue = false;
                recording = true;
                messageBus.firePartRecordStart(id);
            } else if (recording) {
                recording = false;
                playbackBuffer = nextPlaybackBuffer;
                messageBus.firePartRecordFinish(id);
            }
        }

        if (recording) return;

        for (int t = tickInLoop; t <= (tickInLoop + range); t++) {
            List<byte[]> eventsForThisTick = playbackBuffer[t % length];
            for (int evIdx = 0, numEvents = eventsForThisTick.size(); evIdx < numEvents; evIdx++) {
                playMidi(eventsForThisTick.get(evIdx), (t - tickInLoop) + (evIdx * 0.1));
            }
            if(t == length - 1) {
//                playMidi(new byte[] { MidiWire.STATUS_CC, MidiWire.CC_ALL_NOTES_OFF, 0}, (t - tickInLoop) + 0.999);
                playMidi(new byte[]{MidiWire.STATUS_CC, MidiWire.CC_ALL_SOUND_OFF, 0}, (t - tickInLoop) + 0.999);

                playMidi(new byte[]{MidiWire.STATUS_CC, MidiWire.CC_ALL_CONTROLLERS_RESET, 0}, (t - tickInLoop) + 0.999);
            }
        }
    }

    @Override
    public void onMidiInChannelChanged(int partId, int newChannel) {
        if(partId != this.id)
            return;

        this.inputChannel = newChannel;
    }

    @Override
    public void onMidiOutChannelChanged(int partId, int newChannel) {
        if(partId != this.id)
            return;

        this.outputChannel = newChannel;
    }

    public void recordAt(int tickInLoop, byte[] data) {
        if(tickInLoop > length)
            return;

        if(cue) {
            preRecordBuffer[tickInLoop].add(data);
            return;
        }

        playbackBuffer[tickInLoop].add(data);
        if( ! lastRecordRescued)
            rescueEvents();
    }

    private void rescueEvents() {
        for (int i = 0; i < Meter.TICKS_PER_BEAT; i++)
            playbackBuffer[i].addAll(preRecordBuffer[i]);

        // TODO
        int PULL_NOTES_FORWARD_TOLERANCE = TICKS_PER_SEMIQUAVER;

        for (int i = preRecordBuffer.length - 1;
             i >= preRecordBuffer.length - PULL_NOTES_FORWARD_TOLERANCE; i--)
            playbackBuffer[0].addAll(preRecordBuffer[i]);

        lastRecordRescued = true;
    }
}
