package net.poundex.audio.moop.engine.action;

import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.CuePartRecCommand;
import net.poundex.audio.moop.engine.Action;

public enum CuePartRecordAction implements Action<CuePartRecCommand> {
    INSTANCE;

    @Override
    public void accept(EngineContext engineContext, CuePartRecCommand command) {
        engineContext.getLooper().getPart(command.getPartId()).setCue(true);
        engineContext.getMessageBus().firePartRecordCue(command.getPartId());
    }
}
