package net.poundex.audio.moop.engine.looper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.audio.moop.core.messaging.listener.PartRecordCueListener;
import net.poundex.audio.moop.engine.util.ObjectPool;
import net.poundex.audio.moop.engine.util.ObjectPool.RawInboundEvent;

import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

@Slf4j
@RequiredArgsConstructor
public
class EventProcessor implements Runnable, PartRecordCueListener {

    private final BlockingDeque<RawInboundEvent> inboundEventsQueue =
            new LinkedBlockingDeque<>(1024);

    private final Map<Integer, DefaultPart> parts;

    private boolean newPartsAreArmed = false;

    @Override
    public void run() {
        while(true) {
            newPartsAreArmed = false;
            int[] partsRecording = getPartsRecording();

            while (true) {
                try {
                    RawInboundEvent nextEv = inboundEventsQueue.take();
                    if(newPartsAreArmed) {
                        inboundEventsQueue.addFirst(nextEv);
                        break;
                    }

                    for (int pId : partsRecording)
                        parts.get(pId).recordAt(nextEv.getTickInLoop(), nextEv.getData());

                    ObjectPool.rawInboundEvent.offer(nextEv);
                } catch (final Exception ex) {
                    log.error("In process inbound events thread", ex);
                }
            }
        }
    }

    private int[] getPartsRecording() {
        return parts.values().stream()
                .filter(p -> p.isRecording() || p.isCue())
                .mapToInt(DefaultPart::getId)
                .toArray();
    }

    public EventProcessor start() {
        new Thread(this, "inbound-events-processor").start();
        return this;
    }

    public void queueEvent(RawInboundEvent inboundEvent) {
        inboundEventsQueue.add(inboundEvent);
    }

    @Override
    public void onPartRecordCue(int partId) {
        newPartsAreArmed = true;
    }
}
