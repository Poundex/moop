package net.poundex.audio.moop.engine.action;

import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.TransportStartCommand;
import net.poundex.audio.moop.engine.Action;

public enum TransportStartAction implements Action<TransportStartCommand> {
    INSTANCE;

    @Override
    public void accept(EngineContext engineContext, TransportStartCommand command) {
        engineContext.getAudioSystem().transportStart();
    }
}
