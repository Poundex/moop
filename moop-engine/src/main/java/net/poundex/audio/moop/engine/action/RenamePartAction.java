package net.poundex.audio.moop.engine.action;

import net.poundex.audio.moop.core.EngineContext;
import net.poundex.audio.moop.core.command.RenamePartCommand;
import net.poundex.audio.moop.engine.Action;

public enum RenamePartAction implements Action<RenamePartCommand> {
    INSTANCE;

    @Override
    public void accept(EngineContext engineContext, RenamePartCommand command) {
        engineContext.getLooper().getPart(command.getPartId()).setName(command.getNewName());
        engineContext.getMessageBus().firePartRenamed(command.getPartId(), command.getNewName());
    }
}
